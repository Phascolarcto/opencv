TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    ex1inter.cpp
OPENCVDIR3 = "C:/Libs/opencv_320"

CONFIG( release, debug | release ){
    LIBS += -L$${OPENCVDIR3}/build/x64/vc14/lib/ \
                -lopencv_world320

}

CONFIG( debug, debug | release ){
    LIBS += -L$${OPENCVDIR3}/build/x64/vc14/lib/ \
                -lopencv_world320d
}


INCLUDEPATH += $${OPENCVDIR3}/build/include\
               $${OPENCVDIR3}/3rdparty/include\
               $${OPENCVDIR3}/build/include/opencv2\
               $${OPENCVDIR3}/build/include/opencv\
DEPENDPATH += $${OPENCVDIR3}/build/x64/vc14/bin\
