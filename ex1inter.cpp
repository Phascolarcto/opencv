#include <iostream>
#include"opencv2/core.hpp"
#include <opencv2/opencv.hpp>
#include <string.h>
using namespace cv;
using namespace std;
int mathfloor(float val){
    float res;
    float fractpart;
    fractpart = modf(val, &res);
    //res = floor(val);
    if(fractpart - 0.5 > 0){
        res = res + 1;
    }
    return (int)res;
}

float BilInterpol(Mat &src, float x, float y){
   //f(0,0)(1-x)(1-y) + f(1,0)x(1-y) + f(0,1)(1-x)y + f(1,1)xy
    float x_center;
    float y_center;
    float fractpart_x;
    float fractpart_y;
    fractpart_x = modf(x,&x_center);
   // x_center = floor(x);
    if(fractpart_x - 0.5 > 0){
    x_center = x_center + 1;
    fractpart_x = -(fractpart_x - 0.5);
    }
    fractpart_y = modf(y,&y_center);
   // y_center = floor(y);
    if(fractpart_y - 0.5 > 0){
    y_center = y_center + 1;
    fractpart_y = -(fractpart_y - 0.5);
    }
    fractpart_x = fractpart_x + 0.5;
    fractpart_y = fractpart_y + 0.5;
    float f_0_0 = (float)(src.at<unsigned char>(Point((int)x_center - 1, (int)y_center - 1)));
    float f_1_1 = (float)(src.at<unsigned char>(Point((int)x_center, (int)y_center)));
    float f_1_0 = (float)(src.at<unsigned char>(Point((int)x_center, (int)y_center - 1)));
    float f_0_1 = (float)(src.at<unsigned char>(Point((int)x_center - 1, (int)y_center)));
    float res = (f_0_0*(1.0 - fractpart_x)*(1.0-fractpart_y)) + (f_1_0*fractpart_x*(1.0 - fractpart_y)) +
            (f_0_1*(1.0-fractpart_x)*fractpart_y) + (f_1_1*fractpart_x*fractpart_y);
    return res;
}
Point2f GetSrcCoord(float x, float y, float mul_x, float mul_y){
    Point2f res(x*mul_x + 1, y*mul_y + 1);
    return res;
}
Mat ExtImg(Mat&img){
    Size sz_imgExt(img.size[1] + 2, img.size[0] + 2);
    Mat imgExt(sz_imgExt, CV_8UC1, Scalar(0,0,0));
    for(int x = 0; x < img.size[1]; x++){
        for(int y = 0; y < img.size[0]; y++){
               imgExt.at<unsigned char>(Point(x+1,y + 1)) = img.at<unsigned char>(Point(x,y));
        }
    }
    for(int j = 0; j < img.size[1]; j++){
        imgExt.at<unsigned char>(Point(j + 1,1)) = img.at<unsigned char>(Point(j,0));
    }
    for(int j = 0; j < img.size[0]; j++){
        imgExt.at<unsigned char>(Point(img.size[1] - 1,j)) = img.at<unsigned char>(Point(img.size[1] - 1,j));
    }
    for(int j = 0; j < img.size[1]; j++){
        imgExt.at<unsigned char>(Point(j,img.size[0] - 1)) = img.at<unsigned char>(Point(j,img.size[0] - 1));
    }
    for(int j = 0; j < img.size[0]; j++){
        imgExt.at<unsigned char>(Point(1,j)) = img.at<unsigned char>(Point(0,j));
    }
    return imgExt;
}

void main(){
    string name;
    cin >> name;
    int XszNew;
    int YszNew;
    cout << "XsxzNew YszNew \n";
    cin >> XszNew;
    cin >> YszNew;
    Mat img = imread(name, CV_LOAD_IMAGE_UNCHANGED);
    if (img.empty())
    {
        cout << "Error : Image cannot be loaded....." << endl;
        return;
    }
    cvtColor(img, img, COLOR_BGR2GRAY);
    Size sz(XszNew,YszNew);
    Mat I1(sz, CV_8UC1, Scalar(0,0,0));
    resize(img, I1, sz);
    imwrite("I1.bmp", I1);
    Mat I2(sz, CV_8UC1, Scalar(0,0,0));
    float mul_x = (float)img.size[1]/(float)sz.width; //множитель, т.е. во сколько сжимается изображение
    float mul_y = (float)img.size[0]/(float)sz.height;
    Mat imgExt = ExtImg(img);
    float x_crd;
    float y_crd;
    for(int x = 0; x<sz.width; x++){
        for(int y = 0; y<sz.height; y++){
            x_crd = GetSrcCoord((float)x + 0.5,(float)y + 0.5,mul_x,mul_y).x;
            y_crd = GetSrcCoord((float)x + 0.5,(float)y + 0.5,mul_x,mul_y).y;
            I2.at<unsigned char>(Point(x,y)) = mathfloor(BilInterpol(imgExt, x_crd, y_crd));
        }
    }
    float max = 0;
    float realmax = 0;
    float cntr = 0;
    for(int i = 5; i < sz.width-5; i++){
        for(int j = 5; j < sz.height-5; j++){
            max = max + abs((int)(I1.at<unsigned char>(Point(i,j))) - (int)(I2.at<unsigned char>(Point(i,j))));
            cntr = cntr + 1;
            if(abs((int)(I1.at<unsigned char>(Point(i,j))) - (int)(I2.at<unsigned char>(Point(i,j)))) > realmax){
                realmax = abs((int)(I1.at<unsigned char>(Point(i,j))) - (int)(I2.at<unsigned char>(Point(i,j))));
            }
        }
    }

    cout << max/cntr;
    cout << " REAL MAX " << realmax;
    imwrite("I2.bmp", I2);
    waitKey(0);
}
